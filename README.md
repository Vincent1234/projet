# Projet Web


## Un petit cadeau pour aujourd'hui


C'est un menu avec un drop-down (que vous pouvez facilement enlever) et qui est responsive. C'est assez simple mais ça peut dépanner.
__Attention__,  si je mets du code, même si il est à moi, j'exige que vous le compreniez !!!
C'est [ici](https://codepen.io/trazafin/pen/KgRXzB).


## NOTES IMPORTANTES


- __J'ai envoyé un mail avec deux liens vers des vidéos. Les avez-vous regardées ?__
- __Votre projet doit se trouver sur gitlab !!__
- __Si votre code est mal identé, votre projet ne sera pas évalué__

## ATTENTION


- ORGANISEZ BIEN VOS REPERTOIRES.
    - Le fichier `index.html` doit se trouver à la racine.
    - Tous les autres fichiers doivent être rangés dans un repertoire : doc, css, html, images, ... 
    - Vous pouvez créez des sous répertoires en fonction de l'organisation de votre site.
- Faîtes attention à l'indentation de vos fichiers html et css.
- Même si les goûts et les couleurs ne se discutent pas le mauvais goût existe.


## Sujet


Vous êtes maintenant étudiant dans le département Réseaux et télécommunication de l'IUT de La Réunionà Saint-Pierre. 
Et vous venez d'intégrer l'équipe de développement web de ce département.
Vous devez créer un site web pour ce département qui n'en a pas pour le moment (QUELLE HONTE !!!).

Une première enquête a été réalisée pour recueillir les besoins du département. 
Voici un résumé de ce que le chef de département espère.

### Fonctionnalités:

Le site Web doit contenir:
- Une page de présentation du DUT Réseaux et Télécommunication. 
  - Cette page doit être attrayante (avec des encarts:  photos, news, evenements, etc... )
  - Elle doit contenir les objectifs de la formation
  - Parler de l'apprentissage en deuxième année
- une page contenant les photos des professeurs et une petite biographie de chaque professeur.
- La chartre d'utilisation des équipements informatique du département.
- Une page de contact avec adresse, téléphone et formulaire de contact.
- Une page contenant les matières avec un résumé et un lien pour télécharger le PPN


Le site :
- doit s'adapter à plusieurs taille d'écran (tablette, smartphone, ordinateur classique) (responsive)
- ne doit contenir aucune erreur
- doit être écrit en HTML et CSS uniquement : pas de javascript ou autre
- doit s'afficher correctement sur plusieurs navigateurs dont: chrome, firefox, safari (au minimum)


Si il vous manque des documents, merci de prendre contact avec Mr Arnasalom.



## Consignes


Vous allez réaliser un site web qui devra faire appel à toutes vos connaissances acquises pendant les cours M1106 : **HTML, CSS**. **Le projet sera réalisé de manière individuelle** sur 3 séances de TP et en dehors des cours. 

**Vous n’êtes pas autorisé à utiliser de framework**. Et vous devrez être capable de justifier la présence de chaque ligne de code du projet.

## L’évaluation


L’évaluation du projet se fera sur la présentation orale de 5 min (dernière séance de TP) au cours de laquelle vous présenterez à votre responsable (moi) de TP votre site Internet (barème à titre indicatif):

1. Les pages (total: 3) 
    - Acceuil (1)
    - Profs (.5)
    - Contact (.5)
    - Chartre (.5)
    - Matières (.5)
2. La navigation de votre site (total: 4)
    - Les menus (1)
    - Les liens entre les pages(1)
    - La facilité de navigation (1)
    - demonstration de navigation dans le site (1)
3. Le design (total: 2)
    - Le choix des couleurs (.5)
    - Le choix des polices (.5)
    - Les formats/architectures des pages (1)
3. Les fonctionnalités techniques (total: 4)
    - site sans erreur (.5)
    - navigateurs (.5)
    - responsive avec trois tailles d'écran (3)
4. Revues de sources (total : 5)
    - propreté du code html (2.5)
    - propreté du code css (2.5)
1. La participation pendant les séances (2)
    - Votre présence (.5)
    - Votre interaction (profs / étudiant / association) (1.5)

## Les tâches à réaliser


Afin de vous aider à planifier votre travail, voici une liste de tâches (non exhaustives) que vous allez devoir mettre en œuvre :

1.	Traduire le cahier des charges pour imaginer à quoi va ressembler votre site (exemple de page, navigation…)
2.	Conception de la première page et écriture CSS
3.	Ecrire des contenus, validation de la navigation
4.	Test de vos pages, revu de votre code

## Quelques ressources


-   Pour le contenu du texte, vous pouvez utiliser
    <http://fr.lipsum.com/>. Surtout si vous ne voulez pas produire du texte.

-   Pour les images, elles doivent être libre de droit. Vous pouvez en
    trouver sur <http://flickr.com> avec les bonnes options de
    recherche. N’oubliez pas de créditer les auteurs dans tous les cas.
    Si vous avez besoin d’icones: <http://thenounproject.com>

-  N'oubliez pas d'utiliser les validateurs CSS et HTML pour supprimer toutes les erreurs.

-  Vous pouvez vous inspirer des contenus/informations/structures des sites des autres départements RT.

- Pour choisir les couleurs: <http://colorpalettes.net/>

- Pour les fonts: [ici](https://gitlab.com/M1106/projet/blob/master/files/Psychology_Behind_Typeface_large.jpg)

- Dessinez vos pages comme [ici](https://gitlab.com/M1106/projet/blob/master/files/10.website-sketch.jpg), [ici](https://gitlab.com/M1106/projet/blob/master/files/12.website-sketch.jpg) ou [ici](https://gitlab.com/M1106/projet/blob/master/files/21.website-sketch.jpg). Vous pouvez voir [ce site](http://designbeep.com/2013/05/09/30-great-examples-of-web-design-sketches/) 